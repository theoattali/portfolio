<?php

namespace App\Entity;

use App\Entity\Picture;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"article" = "Article", "experience" = "Experience", "project" = "Project"})
 */
class Article
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"create", "edit"})
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $websiteUrl;

    /**
     * @ORM\Column(type="string", length=1500)
     */
    protected $caption;

    /**
     * @ORM\Column(type="date")
     */
    protected $beginingAt;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="article", cascade={"ALL"})
     * @ORM\OrderBy({"placement" = "ASC"})
     * @Assert\Valid()
     */
    protected $pictures;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $endAt;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $updatedBy;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;


    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;
    }

    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    public function getCaption($opt = null)
    {
        $text = $this->caption;
        if ('slice' == $opt && strlen($text) > 250) {
            preg_match('/^(.*?)<br\/?>/', $text, $matches);
            $text = preg_replace("/<br\/?>/", '', $text);
            return $matches[1];
        }
        return $this->caption;
    }

    public function setBeginingAt($beginingAt)
    {
        $this->beginingAt = $beginingAt;
    }

    public function getBeginingAt()
    {
        return $this->beginingAt;
    }

    public function setEndAt($endAt = null)
    {
        $this->endAt = $endAt;
    }

    public function getEndAt()
    {
        return $this->endAt;
    }

    public function getDateInterval($opt = null)
    {
        $begining = $this->beginingAt;
        $end = $this->endAt;

        $dateFormatter = \IntlDateFormatter::create(
            \Locale::getDefault(),
            \IntlDateFormatter::NONE,
            \IntlDateFormatter::NONE,
            \date_default_timezone_get(),
            \IntlDateFormatter::GREGORIAN,
            'MMMM Y'
        );

        if ($end) {
            if ('duration' == $opt) {
                $interval = $begining->diff($end);
                return ceil($interval->format('%a') / 30);
            }

            $begining = $dateFormatter->format($begining);
            $end = $dateFormatter->format($end);

            return ($begining == $end) ? 'Durant '.$end : $begining.' à '.$end;
        }
        return 'Depuis '.$dateFormatter->format($begining);
    }

    public function addPicture(Picture $picture)
    {
        $this->pictures->add($picture);
        $picture->setArticle($this);
    }

    public function removePicture(Picture $picture)
    {
        $this->pictures->removeElement($picture);
        $picture->setArticle($this);
    }

    public function getPictures()
    {
        return $this->pictures;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
