<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity
 */
class Project extends Article
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $tag;

    /**
     * @ORM\ManyToOne(targetEntity="Experience", inversedBy="projects")
     */
    protected $experience;

    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="project")
     */
    protected $documents;


    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function setExperience(Experience $experience)
    {
        $this->experience = $experience;
    }

    public function getExperience()
    {
        return $this->experience;
    }

    public function getDocuments()
    {
        return $this->documents;
    }
}
