<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity
 */
class ExperienceSkill
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Experience", inversedBy="experienceSkills")
     */
    protected $experience;

    /**
     * @ORM\ManyToOne(targetEntity="Skill")
     */
    protected $skill;


    public function getId()
    {
        return $this->id;
    }

    public function setExperience(Experience $experience)
    {
        $this->experience = $experience;
    }

    public function getExperience()
    {
        return $this->experience;
    }

    public function setSkill(Skill $skill)
    {
        $this->skill = $skill;
    }

    public function getSkill()
    {
        return $this->skill;
    }
}
