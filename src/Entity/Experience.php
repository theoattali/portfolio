<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity
 */
class Experience extends Article
{
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"create", "edit"})
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="ExperienceSkill", mappedBy="experience")
     */
    protected $experienceSkills;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="experience")
     */
    protected $projects;


    public function __construct()
    {
        $this->experienceSkills = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    public function setCompany($company)
    {
        $this->company = $company;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getExperienceSkills()
    {
        return $this->experienceSkills;
    }

    public function getProjects()
    {
        return $this->projects;
    }
}
