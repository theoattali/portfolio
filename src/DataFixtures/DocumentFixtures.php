<?php

namespace App\DataFixtures;

use App\DataFixtures\ProjectFixtures;
use App\Entity\Document;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DocumentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'document_1' => array(
                'path' => 'tutosoft.pdf',
                'caption' => 'Tutosoft',
                'placement' => 1,
                'project' => 'project_1',
            ),
            'document_2' => array(
                'path' => 'gestiondesincidents.pdf',
                'caption' => 'Gestion des incidents',
                'placement' => 1,
                'project' => 'project_4',
            ),
            'document_3' => array(
                'path' => 'enseignementsup.pdf',
                'caption' => 'Enseignement Supérieur',
                'placement' => 1,
                'project' => 'project_5',
            ),
            'document_4' => array(
                'path' => 'blogsymfony2.pdf',
                'caption' => 'Blog symfony',
                'placement' => 1,
                'project' => 'project_7',
            ),
            'document_5' => array(
                'path' => 'sitar.pdf',
                'caption' => 'Sitar - Documentation générale',
                'placement' => 1,
                'project' => 'project_8',
            ),
            'document_6' => array(
                'path' => 'profsoft1.pdf',
                'caption' => 'ProfSoft - Documentation fonctionelle',
                'placement' => 1,
                'project' => 'project_10',
            ),
            'document_7' => array(
                'path' => 'profsoft2.pdf',
                'caption' => 'ProfSoft - Documentation technique',
                'placement' => 2,
                'project' => 'project_10',
            ),
            'document_8' => array(
                'path' => 'profsoft3.pdf',
                'caption' => 'ProfSoft - Guide utilisateur',
                'placement' => 3,
                'project' => 'project_10',
            ),
            'document_9' => array(
                'path' => 'veille_redmine.pdf',
                'caption' => 'Veille - Redmine',
                'placement' => 4,
                'project' => 'project_10',
            ),
            'document_10' => array(
                'path' => 'veille_git.pdf',
                'caption' => 'Veille - Git',
                'placement' => 5,
                'project' => 'project_10',
            ),
        );

        foreach ($objects as $key => $object) {
            $document = new Document();
            $document->setPath($object['path']);
            $document->setCaption($object['caption']);
            $document->setPlacement($object['placement']);

            if ($object['project']) {
                $document->setProject($this->getReference($object['project']));
            }

            $manager->persist($document);
            $this->addReference($key, $document);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ProjectFixtures::class,
        );
    }
}
