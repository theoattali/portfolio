<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'project_1' => array(
                'title' => 'Tutosoft',
                'websiteUrl' => '',
                'caption' => "Ce site web propose des tutoriels vidéo sur les produits de Microsoft, il a été créé avec la collaboration de Nicolas Hedadiha et Vincent Edeyer pour une épreuve du baccalauréat.",
                'tag' => 'HTML, CSS, PHP',
                'beginingAt' => new \DateTime('2014-06-25'),
                'endAt' => new \DateTime('2014-06-25'),
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_2' => array(
                'title' => 'Pascal Pinero',
                'websiteUrl' => 'http://pascalpinero.com',
                'caption' => "Galeries d'images du photographe Pascal Pinéro. <br/><br/> Ce site vitrine eu sa première version en mars 2014. Les technologies utilisées étaient le HTML, CSS, jQuery, PHP. <br/> En mai 2016, une refonte a été éffectuée en utilisant le framework Symfony3.",
                'tag' => 'Symfony3',
                'beginingAt' => new \DateTime('2014-03-01'),
                'endAt' => new \DateTime('2016-05-01'),
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_3' => array(
                'title' => 'DongDong Event',
                'websiteUrl' => 'http://www.dongdong-event.com',
                'caption' => "Ce site web présente l'événement Dong Dong (Festival/Convention) organisé chaque année par Kaiguan-Culture.",
                'tag' => 'WordPress, HTML, CSS, JS',
                'beginingAt' => new \DateTime('2014-08-31'),
                'endAt' => new \DateTime('2014-08-31'),
                'experience' => 'experience_3',

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_4' => array(
                'title' => 'Gestion des incidents',
                'websiteUrl' => 'http://gestiondesincidents.theoattali.fr',
                'caption' => "Ce site web propose une interface de gestion des incidents pour le lycée Saint-Vincent, il a été créé avec la collaboration de Benjamin Kazmierczak et Thibaut Penhard.",
                'tag' => 'HTML, CSS, PHP, jQuery',
                'beginingAt' => new \DateTime('2015-04-10'),
                'endAt' => new \DateTime('2015-04-10'),
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_5' => array(
                'title' => 'Enseignement Supérieur - Senlis',
                'websiteUrl' => 'http://enseignement-superieur.lycee-stvincent.fr',
                'caption' => "Site de l'enseignement supérieur du Lycée Saint-Vincent <br/> - TMA sur Drupal7 dans le cadre d'un stage chez Nodevo.",
                'tag' => 'HTML, CSS, Drupal7',
                'beginingAt' => new \DateTime('2015-06-15'),
                'endAt' => new \DateTime('2015-06-15'),
                'experience' => 'experience_2',

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_6' => array(
                'title' => 'Gestion des cours',
                'websiteUrl' => 'http://gestiondescours.alwaysdata.net',
                'caption' => "Application permettant au professeurs de gérer leurs classes. Ce projet de BTS réalisé avec la collaboration de Grégoire Rondet.",
                'tag' => 'HTML, CSS, jQuery, Ajax, PHP',
                'beginingAt' => new \DateTime('2015-08-31'),
                'endAt' => new \DateTime('2015-08-31'),
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_7' => array(
                'title' => 'Blog Symfony 2.7',
                'websiteUrl' => '',
                'caption' => "Blog réalisé avec Symfony 2.7 dans une optique d'apprentissage chez Nodevo.",
                'tag' => 'Symfony 2.7',
                'beginingAt' => new \DateTime('2015-06-15'),
                'endAt' => new \DateTime('2015-06-15'),
                'experience' => 'experience_2',

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_8' => array(
                'title' => 'Sitar',
                'websiteUrl' => '',
                'caption' => "Cette application a été réalisée avec le framework Symfony 3.0 durant mon stage de 2ème année de BTS.",
                'tag' => 'Symfony 3.0',
                'beginingAt' => new \DateTime('2016-01-04'),
                'endAt' => new \DateTime('2016-01-04'),
                'experience' => 'experience_1',

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'project_9' => array(
                'title' => 'Olivier Froidefond',
                'websiteUrl' => 'http://olivierfroidefond.com',
                'caption' => "Portfolio d'images du photographe Olivier Froidefond.",
                'tag' => 'Symfony 3.0',
                'beginingAt' => new \DateTime('2016-03-01'),
                'endAt' => new \DateTime('2016-05-01'),
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-06-01 11:04:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-06-01 11:04:10'),
            ),
            'project_10' => array(
                'title' => 'Profsoft',
                'websiteUrl' => '',
                'caption' => "Application web permmettant aux enseignants de gérer leur emploi du temps et les notes de leurs élèves.",
                'tag' => 'Symfony 3.0',
                'beginingAt' => new \DateTime('2016-03-01'),
                'endAt' => new \DateTime('2016-06-01'),
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-06-01 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-06-01 11:06:10'),
            ),
            'project_11' => array(
                'title' => 'Portfolio - Site personnel',
                'websiteUrl' => 'http://theoattali.fr',
                'caption' => "Site web personnel présentant mon parcours. <br/> Ce site web m'a permis de mettre en application les technologies que je découvre. <br/> <br/> <strong>Première version :</strong> HTML, CSS, PHP, MYSQL <br/> <strong>Deuxième version :</strong>  HTML, CSS, PHP, MYSQL, JS/jQuery <br/> <strong>Troisième version :</strong> Utilisation du template <a href='http://alvarotrigo.com/fullPage/'>fullpage.js</a> <br/> <strong>Dernière version :</strong> Utilisation du framework Symfony3",
                'tag' => 'Symfony 3.0',
                'beginingAt' => new \DateTime('2014-04-01'),
                'endAt' => null,
                'experience' => null,

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-06-01 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-06-01 11:06:10'),
            ),
        );

        foreach ($objects as $key => $object) {
            $project = new Project();
            $project->setTitle($object['title']);
            $project->setWebsiteUrl($object['websiteUrl']);
            $project->setCaption($object['caption']);
            $project->setTag($object['tag']);
            $project->setBeginingAt($object['beginingAt']);
            $project->setEndAt($object['endAt']);

            if ($object['experience']) {
                $project->setExperience($this->getReference($object['experience']));
            }

            $project->setCreatedBy($object['createdBy']);
            $project->setCreatedAt($object['createdAt']);
            $project->setUpdatedBy($object['updatedBy']);
            $project->setUpdatedAt($object['updatedAt']);

            $manager->persist($project);
            $this->addReference($key, $project);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ExperienceFixtures::class,
        );
    }
}
