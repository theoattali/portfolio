<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'picture_1' => array(
                'path' => 'aftral.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'experience_1',
            ),
            'picture_2' => array(
                'path' => 'nodevo.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'experience_2',
            ),
            'picture_3' => array(
                'path' => 'kaiguan-culture.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'experience_3',
            ),

            'picture_4' => array(
                'path' => 'tutosoft1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_1',
            ),
            'picture_5' => array(
                'path' => 'tutosoft2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_1',
            ),
            'picture_6' => array(
                'path' => 'tutosoft3.png',
                'caption' => null,
                'placement' => 3,
                'article' => 'project_1',
            ),
            'picture_7' => array(
                'path' => 'tutosoft4.png',
                'caption' => null,
                'placement' => 4,
                'article' => 'project_1',
            ),
            'picture_8' => array(
                'path' => 'pascalpinero1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_2',
            ),
            'picture_9' => array(
                'path' => 'pascalpinero2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_2',
            ),
            'picture_10' => array(
                'path' => 'pascalpinero3.png',
                'caption' => null,
                'placement' => 3,
                'article' => 'project_2',
            ),
            'picture_11' => array(
                'path' => 'pascalpinero4.png',
                'caption' => null,
                'placement' => 4,
                'article' => 'project_2',
            ),
            'picture_12' => array(
                'path' => 'pascalpinero5.png',
                'caption' => null,
                'placement' => 5,
                'article' => 'project_2',
            ),
            'picture_13' => array(
                'path' => 'dongdong1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_3',
            ),
            'picture_14' => array(
                'path' => 'dongdong2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_3',
            ),
            'picture_15' => array(
                'path' => 'dongdong3.png',
                'caption' => null,
                'placement' => 3,
                'article' => 'project_3',
            ),
            'picture_16' => array(
                'path' => 'dongdong4.png',
                'caption' => null,
                'placement' => 4,
                'article' => 'project_3',
            ),
            'picture_17' => array(
                'path' => 'dongdong5.png',
                'caption' => null,
                'placement' => 5,
                'article' => 'project_3',
            ),
            'picture_18' => array(
                'path' => 'gestiondesincidents1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_4',
            ),
            'picture_19' => array(
                'path' => 'enseignementsup1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_5',
            ),
            'picture_20' => array(
                'path' => 'gestiondescours1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_6',
            ),
            'picture_21' => array(
                'path' => 'blogsymfony21.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_7',
            ),
            'picture_22' => array(
                'path' => 'sitar1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_8',
            ),
            'picture_23' => array(
                'path' => 'sitar2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_8',
            ),
            'picture_24' => array(
                'path' => 'sitar3.png',
                'caption' => null,
                'placement' => 3,
                'article' => 'project_8',
            ),
            'picture_25' => array(
                'path' => 'sitar4.png',
                'caption' => null,
                'placement' => 4,
                'article' => 'project_8',
            ),
            'picture_26' => array(
                'path' => 'olivierfroidefond1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_9',
            ),
            'picture_27' => array(
                'path' => 'olivierfroidefond2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_9',
            ),
            'picture_28' => array(
                'path' => 'profsoft.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_10',
            ),
            'picture_29' => array(
                'path' => 'profsoft2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_10',
            ),
            'picture_30' => array(
                'path' => 'profsoft3.png',
                'caption' => null,
                'placement' => 3,
                'article' => 'project_10',
            ),
            'picture_31' => array(
                'path' => 'profsoft4.png',
                'caption' => null,
                'placement' => 4,
                'article' => 'project_10',
            ),
            'picture_32' => array(
                'path' => 'portfolio1.png',
                'caption' => null,
                'placement' => 1,
                'article' => 'project_11',
            ),
            'picture_33' => array(
                'path' => 'portfolio2.png',
                'caption' => null,
                'placement' => 2,
                'article' => 'project_11',
            ),
            'picture_34' => array(
                'path' => 'portfolio3.png',
                'caption' => null,
                'placement' => 3,
                'article' => 'project_11',
            ),
            'picture_35' => array(
                'path' => 'portfolio4.png',
                'caption' => null,
                'placement' => 4,
                'article' => 'project_11',
            ),
            'picture_36' => array(
                'path' => 'portfolio5.png',
                'caption' => null,
                'placement' => 5,
                'article' => 'project_11',
            ),
        );

        foreach ($objects as $key => $object) {
            $picture = new Picture();
            $picture->setPath($object['path']);
            $picture->setCaption($object['caption']);
            $picture->setPlacement($object['placement']);

            if ($object['article']) {
                $picture->setArticle($this->getReference($object['article']));
            }

            $manager->persist($picture);
            $this->addReference($key, $picture);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ExperienceFixtures::class,
            ProjectFixtures::class,
        );
    }
}
