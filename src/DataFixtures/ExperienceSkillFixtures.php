<?php

namespace App\DataFixtures;

use App\DataFixtures\ExperienceFixtures;
use App\DataFixtures\SkillFixtures;
use App\Entity\ExperienceSkill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ExperienceSkillFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'experienceSkill_1' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_1',
            ),
            'experienceSkill_2' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_2',
            ),
            'experienceSkill_3' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_3',
            ),
            'experienceSkill_4' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_7',
            ),
            'experienceSkill_5' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_13',
            ),
            'experienceSkill_6' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_25',
            ),
            'experienceSkill_7' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_26',
            ),
            'experienceSkill_8' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_27',
            ),
            'experienceSkill_8' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_32',
            ),
            'experienceSkill_8' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_33',
            ),
            'experienceSkill_8' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_34',
            ),
            'experienceSkill_8' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_46',
            ),
            'experienceSkill_8' => array(
                'experience' => 'experience_1',
                'skill' => 'skill_48',
            ),
        );

        foreach ($objects as $key => $object) {
            $experienceSkill = new ExperienceSkill();

            if ($object['experience']) {
                $experienceSkill->setExperience($this->getReference($object['experience']));
            }

            if ($object['skill']) {
                $experienceSkill->setSkill($this->getReference($object['skill']));
            }

            $manager->persist($experienceSkill);
            $this->addReference($key, $experienceSkill);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ExperienceFixtures::class,
            SkillFixtures::class,
        );
    }
}
