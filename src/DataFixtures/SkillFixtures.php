<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SkillFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'skill_1' => array(
                'code' => 'A1.1.1',
                'label' => 'Analyse du cahier des charges d\'un service à produire',
            ),
            'skill_2' => array(
                'code' => 'A1.1.2',
                'label' => 'Étude de l\'impact de l\'intégration d\'un service sur le système informatique',
            ),
            'skill_3' => array(
                'code' => 'A1.1.3',
                'label' => 'Étude des exigences liées à la qualité attendue d\'un service',
            ),
            'skill_4' => array(
                'code' => 'A1.2.1',
                'label' => 'Élaboration et présentation d\'un dossier de choix de solution technique',
            ),
            'skill_5' => array(
                'code' => 'A1.2.2',
                'label' => 'Rédaction des spécifications techniques de la solution retenue',
            ),
            'skill_6' => array(
                'code' => 'A1.2.3',
                'label' => 'Évaluation des risques liés à l\'utilisation d\'un service',
            ),
            'skill_7' => array(
                'code' => 'A1.2.4',
                'label' => 'Détermination des tests nécessaires à la validation d\'un service',
            ),
            'skill_8' => array(
                'code' => 'A1.2.5',
                'label' => 'Définition des niveaux d\'habilitation associés à un service',
            ),
            'skill_9' => array(
                'code' => 'A1.3.1',
                'label' => 'Test d\'intégration et d\'acceptation d\'un service',
            ),
            'skill_10' => array(
                'code' => 'A1.3.2',
                'label' => 'Définition des éléments nécessaires à la continuité d\'un service',
            ),
            'skill_11' => array(
                'code' => 'A1.3.3',
                'label' => 'Accompagnement de la mise en place d\'un nouveau service',
            ),
            'skill_12' => array(
                'code' => 'A1.3.4',
                'label' => 'Déploiement d\'un service',
            ),
            'skill_13' => array(
                'code' => 'A1.4.1',
                'label' => 'Participation à un projet',
            ),
            'skill_14' => array(
                'code' => 'A1.4.2',
                'label' => 'Évaluation des indicateurs de suivi d\'un projet et justification des écarts',
            ),
            'skill_15' => array(
                'code' => 'A1.4.3',
                'label' => 'Gestion des ressources',
            ),
            'skill_16' => array(
                'code' => 'A2.1.1',
                'label' => 'Accompagnement des utilisateurs dans la prise en main d\'un service',
            ),
            'skill_17' => array(
                'code' => 'A2.1.2',
                'label' => 'Évaluation et maintien de la qualité d\'un service',
            ),
            'skill_18' => array(
                'code' => 'A2.2.1',
                'label' => 'Suivi et résolution d\'incidents',
            ),
            'skill_19' => array(
                'code' => 'A2.2.2',
                'label' => 'Suivi et réponse à des demandes d\'assistance',
            ),
            'skill_20' => array(
                'code' => 'A2.2.3',
                'label' => 'Réponse à une interruption de service',
            ),
            'skill_21' => array(
                'code' => 'A2.3.1',
                'label' => 'Identification, qualification et évaluation d\'un problème',
            ),
            'skill_22' => array(
                'code' => 'A2.3.2',
                'label' => 'Proposition d\'amélioration d\'un service',
            ),
            'skill_23' => array(
                'code' => 'A3.2.1',
                'label' => 'Installation et configuration d\'éléments d\'infrastructure',
            ),
            'skill_24' => array(
                'code' => 'A3.2.2',
                'label' => 'Remplacement ou mise à jour d\'éléments défectueux ou obsolètes',
            ),
            'skill_25' => array(
                'code' => 'A4.1.1',
                'label' => 'Proposition d\'une solution applicative',
            ),
            'skill_26' => array(
                'code' => 'A4.1.2',
                'label' => 'Conception ou adaptation de l\'interface utilisateur d\'une solution applicative',
            ),
            'skill_27' => array(
                'code' => 'A4.1.3',
                'label' => 'Conception ou adaptation d\'une base de données',
            ),
            'skill_28' => array(
                'code' => 'A4.1.4',
                'label' => 'Définition des caractéristiques d\'une solution applicative',
            ),
            'skill_29' => array(
                'code' => 'A4.1.5',
                'label' => 'Prototypage de composants logiciels',
            ),
            'skill_30' => array(
                'code' => 'A4.1.6',
                'label' => 'Gestion d\'environnements de développement et de test',
            ),
            'skill_31' => array(
                'code' => 'A4.1.7',
                'label' => 'Développement, utilisation ou adaptation de composants logiciels',
            ),
            'skill_32' => array(
                'code' => 'A4.1.8',
                'label' => 'Réalisation des tests nécessaires à la validation d\'éléments adaptés ou développés',
            ),
            'skill_33' => array(
                'code' => 'A4.1.9',
                'label' => 'Rédaction d\'une documentation technique',
            ),
            'skill_34' => array(
                'code' => 'A4.1.10',
                'label' => 'Rédaction d\'une documentation d\'utilisation',
            ),
            'skill_35' => array(
                'code' => 'A4.2.1',
                'label' => 'Analyse et correction d\'un dysfonctionnement, d\'un problème de qualité de …',
            ),
            'skill_36' => array(
                'code' => 'A4.2.2',
                'label' => 'Adaptation d\'une solution applicative aux évolutions de ses composants',
            ),
            'skill_37' => array(
                'code' => 'A4.2.3',
                'label' => 'Réalisation des tests nécessaires à la mise en production d\'éléments mis à jour',
            ),
            'skill_38' => array(
                'code' => 'A4.2.4',
                'label' => 'Mise à jour d\'une documentation technique',
            ),
            'skill_39' => array(
                'code' => 'A5.1.1',
                'label' => 'Mise en place d\'une gestion de configuration',
            ),
            'skill_40' => array(
                'code' => 'A5.1.2',
                'label' => 'Recueil d\'informations sur une configuration et ses éléments',
            ),
            'skill_41' => array(
                'code' => 'A5.1.3',
                'label' => 'Suivi d\'une configuration et de ses éléments',
            ),
            'skill_42' => array(
                'code' => 'A5.1.4',
                'label' => 'Étude de propositions de contrat de service (client, fournisseur)',
            ),
            'skill_43' => array(
                'code' => 'A5.1.5',
                'label' => 'Évaluation d\'un élément de configuration ou d\'une configuration',
            ),
            'skill_44' => array(
                'code' => 'A5.1.6',
                'label' => 'Évaluation d\'un investissement informatique',
            ),
            'skill_45' => array(
                'code' => 'A5.2.1',
                'label' => 'Exploitation des référentiels, normes et standards adoptés par le prestataire',
            ),
            'skill_46' => array(
                'code' => 'A5.2.2',
                'label' => 'Veille technologique',
            ),
            'skill_47' => array(
                'code' => 'A5.2.3',
                'label' => 'Repérage des compléments de formation ou d\'auto-formation ...',
            ),
            'skill_48' => array(
                'code' => 'A5.2.4',
                'label' => 'Étude d˜une technologie, d\'un composant, d\'un outil ou d\'une méthode',
            ),
        );

        foreach ($objects as $key => $object) {
            $skill = new Skill();
            $skill->setCode($object['code']);
            $skill->setLabel($object['label']);

            $manager->persist($skill);
            $this->addReference($key, $skill);
        }

        $manager->flush();
    }
}
