<?php

namespace App\DataFixtures;

use App\Entity\Experience;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ExperienceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'experience_1' => array(
                'title' => 'Stagiaire - Développement Symfony3',
                'company' => 'AFT Services',
                'imgUrl' => 'assets/experience/1/aftral.png',
                'websiteUrl' => 'http://www.aftral.com',
                'caption' => "L'<a href='http://www.aftral.com/'>AFTRAL</a> est une entreprise qui propose des formations en Transport et Logistique à travers son réseau de centres de formation dans toute la France.<br/><br/>Mon stage de 2 mois dans cette entreprise, était au service informatique de l'entreprise aussi appelé AFT Services. Pendant ce stage, ma principale tâche à été de développer une application web permettant de gérer les sites (Infrastructures, centres, régions …) de l'entreprise.<br/><br/>Les principales missions qui mon été confiées dans ce projet sont :<br/>- Créer, rechercher, consulter, modifier, clôturer, consulter l'historique de la fiche<br/>- Authentification via un service centralisé<br/>- Web Services - Obtenir une fiche et Rechercher des fiches (protocole REST)<br/>- Diffuser les logs à Kibana<br/>- Mise en place des tests de non regression",
                'beginingAt' => new \DateTime('2016-01-04'),
                'endAt' => new \DateTime('2016-02-26'),

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'experience_2' => array(
                'title' => 'Stagiaire - Développement Web',
                'company' => 'Nodevo',
                'imgUrl' => 'assets/experience/2/nodevo.png',
                'websiteUrl' => 'http://www.nodevo.com',
                'caption' => "<a href='http://www.nodevo.com/'>Nodevo</a> est une SSII fondé en 2007. L'entreprise propose des projets sur-mesure pour ses clients.<br/><br/>Pour mon stage de première année de BTS, j'ai eu pour mission de :<br/>- TMA sur Drupal 7 (Opération de maintenance sur le site : http://enseignement-superieur.lycee-stvincent.fr) <br/>- Réalisation d'un blog avec le framework Symfony 2.7",
                'beginingAt' => new \DateTime('2015-06-01'),
                'endAt' => new \DateTime('2015-06-26'),

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
            'experience_3' => array(
                'title' => 'Stagiaire - Création d\'un site vitrine',
                'company' => 'Kaiguan-Culture',
                'imgUrl' => 'assets/experience/3/kaiguan.png',
                'websiteUrl' => 'http://www.kaiguan-culture.com',
                'caption' => "<a href='http://www.kaiguan-culture.com/'>Kaiguan-Culture</a> est une entreprise spécialisée dans la création d'événements de culture contemporaine et musicaux en Chine. <br/><br/>Afin de présenter le festival et convention, DongDong, il m'a été confié de créer un site vitrine avec le CMS WordPress.",
                'beginingAt' => new \DateTime('2014-08-4'),
                'endAt' => new \DateTime('2014-08-29'),

                'createdBy' => 'admin',
                'createdAt' => new \DateTime('2016-02-24 11:06:10'),
                'updatedBy' => 'admin',
                'updatedAt' => new \DateTime('2016-02-24 11:06:10'),
            ),
        );

        foreach ($objects as $key => $object) {
            $experience = new Experience();
            $experience->setTitle($object['title']);
            $experience->setCompany($object['company']);
            $experience->setWebsiteUrl($object['websiteUrl']);
            $experience->setCaption($object['caption']);
            $experience->setBeginingAt($object['beginingAt']);
            $experience->setEndAt($object['endAt']);

            $experience->setCreatedBy($object['createdBy']);
            $experience->setCreatedAt($object['createdAt']);
            $experience->setUpdatedBy($object['updatedBy']);
            $experience->setUpdatedAt($object['updatedAt']);

            $manager->persist($experience);
            $this->addReference($key, $experience);
        }

        $manager->flush();
    }
}
