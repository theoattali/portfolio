<?php

namespace App\Controller;

use App\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="post_")
 */
class PostController extends Controller
{
    /**
     * @Route(
     *     "/{type}",
     *     name="index",
     *     defaults={"type": null},
     *     requirements={
     *         "type"="project|experience"
     *     }
     * )
     */
    public function indexAction($type = null)
    {
        $em = $this->getDoctrine();
        $experiences = $em->getRepository(Entity\Experience::class)
            ->findBy(array(), array(
                'endAt' => 'DESC',
            ))
        ;
        $projects = $em->getRepository(Entity\Project::class)
            ->findBy(array(), array(
                'beginingAt' => 'DESC',
                'endAt' => 'DESC',
            ))
        ;

        if ($type) {
            return $this->render('post/list.html.twig', array(
                'posts' => ('experience' == $type) ? $experiences : $projects,
                'type' => $type,
            ));
        }

        $posts = array(
            'experiences' => $experiences,
            'projects' => $projects,
        );

        return $this->render('post/index.html.twig', array(
            'posts' => $posts,
        ));
    }

    /**
     * @Route(
     *     "/{type}/{id}/",
     *     name="show",
     *     requirements={
     *         "type"="project|experience",
     *         "id"="\d+"
     *     }
     * )
     */
    public function showAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Entity\Post::class)->findOneById($id);

        if (!$post) {
            throw $this->createNotFoundException(sprintf('L\'post %d n\'a pas été trouvée', $id));
        }

        return $this->render('post/show.html.twig', array(
            'post' => $post,
            'type' => $type,
        ));
    }
}
