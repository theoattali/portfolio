<?php

namespace App\Controller;

use App\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="article_")
 */
class ArticleController extends Controller
{
    /**
     * @Route(
     *     "/{type}",
     *     name="index",
     *     defaults={"type": null},
     *     requirements={
     *         "type"="project|experience"
     *     }
     * )
     */
    public function indexAction($type = null)
    {
        $em = $this->getDoctrine();
        $experiences = $em->getRepository(Entity\Experience::class)
            ->findBy(array(), array(
                'endAt' => 'DESC',
            ))
        ;
        $projects = $em->getRepository(Entity\Project::class)
            ->findBy(array(), array(
                'beginingAt' => 'DESC',
                'endAt' => 'DESC',
            ))
        ;

        if ($type) {
            return $this->render('article/list.html.twig', array(
                'articles' => ('experience' == $type) ? $experiences : $projects,
                'type' => $type,
            ));
        }

        $articles = array(
            'experiences' => $experiences,
            'projects' => $projects,
        );

        return $this->render('article/index.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * @Route(
     *     "/{type}/{id}/",
     *     name="show",
     *     requirements={
     *         "type"="project|experience",
     *         "id"="\d+"
     *     }
     * )
     */
    public function showAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Entity\Article::class)->findOneById($id);

        if (!$article) {
            throw $this->createNotFoundException(sprintf('L\'article %d n\'a pas été trouvée', $id));
        }

        return $this->render('article/show.html.twig', array(
            'article' => $article,
            'type' => $type,
        ));
    }
}
